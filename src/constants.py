import os

PORT = int(os.environ.get('CASSANDRA_PORT'))
PASSWORD = os.environ.get('CASSANDRA_PASSWORD')
USERNAME = os.environ.get('CASSANDRA_USERNAME')
CASSANDRA_SEEDS = os.environ.get('CASSANDRA_SEEDS')
KEYSPACE = os.environ.get('CASSANDRA_KEYSPACE')
