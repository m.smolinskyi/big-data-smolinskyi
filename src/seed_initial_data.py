seeding_fav_songs = [
    (3, 'Grave Digger', 'The Ballad Of Mary', '2006',),
    (4, 'Linkin Park', 'Given Up', '2007',)
]

seeding_fav_movies = [
    (1, 'Inception', 'Christopher Nolan', '2010',),
    (4, 'Rebel Moon: Part One - A Child of Fire', 'Zack Snyder', '2023',)
]


def seed_data(song_model, movie_model):
    # Seeding of favorite_songs table
    for fav_song in seeding_fav_songs:
        (uid, author, song_name, release_year) = fav_song
        song_model.create(uid, author, song_name, release_year)

    # Seeding of favorite_movies table
    for fav_movie in seeding_fav_movies:
        (uid, name, producer, release_year) = fav_movie
        movie_model.create(uid, name, producer, release_year)
