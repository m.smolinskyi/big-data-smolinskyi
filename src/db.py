import socket

from cassandra.cluster import Cluster, Session
from cassandra.auth import PlainTextAuthProvider

from constants import CASSANDRA_SEEDS, KEYSPACE, PASSWORD, PORT, USERNAME


def is_open(ip, port):
    test = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        test.connect((ip, port))
        test.shutdown(1)
        return True
    except:
        return False


def get_db_ips_list():
    ips = []
    for ip in CASSANDRA_SEEDS.split(','):
        if is_open(ip, PORT):
            ips.append(ip)
    return ips


class DB:
    def __init__(self):
        self.cluster = Cluster(get_db_ips_list(), port=PORT, auth_provider=PlainTextAuthProvider(
            username=USERNAME, password=PASSWORD))

        self.session: Session = self.cluster.connect()

    def init(self):
        # Creating of keyspace
        self.session.execute("""
        CREATE KEYSPACE IF NOT EXISTS %s
        WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
         """ % KEYSPACE)

        # Using of created keyspace
        self.session = self.cluster.connect(KEYSPACE)

        # Creating of table favorite_songs in keyspace
        self.session.execute("""
            CREATE TABLE IF NOT EXISTS favorite_songs (
                id int PRIMARY KEY,
                author text,
                song_name text,
                release_year text
            )""")

        # Creating of favorite_movies favorite_songs in keyspace
        self.session.execute("""
            CREATE TABLE IF NOT EXISTS favorite_movies (
                id int PRIMARY KEY,
                name text,
                producer text,
                release_year text 
            )""")
