from flask import Flask

from db import DB
from models.movie_model import MovieModel
from models.song_model import SongModel
from seed_initial_data import seed_data

app = Flask(__name__)
db = DB()
db.init()

songModel = SongModel(db)
movieModel = MovieModel(db)


def seed_init_data():
    seed_data(song_model=songModel, movie_model=movieModel)


@app.route('/', methods=['GET'])
def get_all():
    songs = songModel.get_all()
    movies = movieModel.get_all()
    return {
        "fav_songs": songs,
        "fav_movies": movies
    }


if __name__ == '__main__':
    seed_init_data()

    app.run(debug=True, host='0.0.0.0', port=5555)
