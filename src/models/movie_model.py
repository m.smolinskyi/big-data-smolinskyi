from cassandra.query import SimpleStatement
from cassandra import ConsistencyLevel


class MovieModel:
    def __init__(self, db):
        self.db = db
        self.create_query: SimpleStatement = SimpleStatement("""
            INSERT INTO hw2_smolinskyi.favorite_movies (id, name, producer, release_year)
            VALUES (%(id)s, %(name)s, %(producer)s, %(release_year)s)
            """, consistency_level=ConsistencyLevel.ONE)

    def create(self, uid, name, producer, release_year):
        self.db.session.execute(
            self.create_query,
            dict(id=uid, name=name, producer=producer, release_year=release_year)
        )

    def get_all(self):
        rows = self.db.session.execute('SELECT * FROM favorite_movies')
        movies = []

        if rows is not None:
            for row in rows:
                movies.append(MovieModel.serialize(row))

        return movies

    @staticmethod
    def serialize(row):
        return dict({
            'id': row.id,
            'name': row.name,
            'producer': row.producer,
            'release_year': row.release_year,
        })
