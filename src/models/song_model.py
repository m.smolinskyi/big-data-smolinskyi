from cassandra.query import SimpleStatement
from cassandra import ConsistencyLevel


class SongModel:
    def __init__(self, db):
        self.db = db
        self.create_query: SimpleStatement = SimpleStatement("""
            INSERT INTO hw2_smolinskyi.favorite_songs (id, author, song_name, release_year)
            VALUES (%(id)s, %(author)s, %(song_name)s, %(release_year)s)
            """, consistency_level=ConsistencyLevel.ONE)

    def create(self, uid, author, song_name, release_year):
        self.db.session.execute(
            self.create_query,
            dict(id=uid, author=author, song_name=song_name,
                 release_year=release_year)
        )

    def get_all(self):
        rows = self.db.session.execute('SELECT * FROM favorite_songs')
        songs = []

        if rows is not None:
            for row in rows:
                songs.append(SongModel.serialize(row))

        return songs


    @staticmethod
    def serialize(row):
        return dict({
            'id': row.id,
            'author': row.author,
            'song_name': row.song_name,
            'release_year': row.release_year,
        })
